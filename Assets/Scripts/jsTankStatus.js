﻿#pragma strict

static var myHp = 0;			// 현재 내 체력.
static var enemyHp = 0;			// 적 체력..
static final var MAXHP = 500;	// 최대 체력. final 은 값의 변경을 막기 위함.
var style : GUIStyle;

function Start()
{
	style.normal.textColor = Color.black;		// 글씨 색 검정색으로 변경.
	
	// 게임 시작 체력.
	myHp = MAXHP;
	enemyHp = MAXHP;
}

function OnGUI()
{
	GUI.Label( Rect( 10, 10, 120, 20 ), "내 체력 : " + myHp, style );
	GUI.Label( Rect( 10, 30, 120, 20 ), "적 체력 : " + enemyHp, style );
}