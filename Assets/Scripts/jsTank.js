﻿#pragma strict

var speed = 5;				// 이동 속도( m/s ).
var rotSpeed = 120;			// 회전 속도( 각도/s ).
var turret : GameObject;	// 포탑.

var power = 600;			// 포탄 바라 속도.
var bullet : Transform;		// 포탄 추가.
var explosion : Transform;
var snd : AudioClip;

var ang2 = 0.0;
var ang3 = 0.0;
var horAngle = 0.0;			// 포탑 수평 회전각.
var verAngle = 0.0;			// 포탑 수직 회전각.

function Start () {

}

function Update () {
	var amtToMove = speed * Time.deltaTime;						// 프레임에 이동할 거리.
	var amtToRot = rotSpeed * Time.deltaTime;					// 프레임에 회전할 각도.

	var front = Input.GetAxis( "Vertical" );					// 전 후진.
	var ang = Input.GetAxis( "Horizontal" );					// 좌우 회전 방향(벡터).
	ang2 = Input.GetAxis( "MyTankHor" );
	ang3 = Input.GetAxis( "MyTankVer" );
	
	
	transform.Translate( Vector3.forward * front * amtToMove );					// 탱크 전후진.
	transform.Rotate( Vector3( 0, ang * amtToRot, 0 ) );						// 탱크 회전.
	turret.transform.Rotate( Vector3.up * HorizonAngleLimit() * amtToRot );		// 포탑 수평 회전.
	turret.transform.Rotate( Vector3.right * VerticalAngleLimit() * amtToRot );	// 포탑 수직 회전.
	
	/* localRotation 을 사용 할 경우에는 상하로는 움직이는데 좌우로는 안움직임. 이유는 모르겟..하핳ㅎ..
	horAngle += Input.GetAxis( "MyTankHor" ) * amtToRot;
	horAngle = Mathf.Clamp( horAngle, -120, 120 );
	
	verAngle += Input.GetAxis( "MyTankVer" ) * amtToRot;
	verAngle = Mathf.Clamp( verAngle, -45, 0 );					// 회전에 제한을 둠. -45~0 까지만 움직인다.
	turret.transform.localRotation = Quaternion.AngleAxis( horAngle, Vector3.up );		// 왜.. 안움직일까요..
	turret.transform.localRotation = Quaternion.AngleAxis( verAngle, Vector3.right );	// Vector3.right 를 기준으로 verAngle 만큼 회전.
	*/
	
	// 포탄 발사 추가.
	if( Input.GetButtonDown( "Fire1" ) )
	{
		var spPoint = GameObject.Find( "spawnPoint" );		// spwanPoint 정보 읽기.
		Instantiate( explosion, spPoint.transform.position, Quaternion.identity );
		AudioSource.PlayClipAtPoint( snd, spPoint.transform.position );
		var myBullet = Instantiate( bullet,
		spPoint.transform.position, spPoint.transform.rotation );
		myBullet.rigidbody.AddForce( turret.transform.forward * power );
	}
}

// 수평 회전각 제한 걸어주는 함수.
function HorizonAngleLimit() : float
{
// 허용 범위 내의 회전각이라면 
	if( horAngle <= 45 && ang2 > 0 )	
	{
		// 회전 허용.
		horAngle += ang2;
		return ang2;
	}
	else if( horAngle >= -45 && ang2 < 0 )
	{
		// 회전 허용.
		horAngle += ang2;
		return ang2;
	}
// 허용 범위 밖의 회전각이라면
	else
	{
		// 회전 허용x.
		return 0;
	}
}

// 수직 회전각 제한 걸어주는 함수.
function VerticalAngleLimit() : float
{
	if( verAngle <= 5 && ang3 > 0 )	
	{
		// 회전 허용.
		verAngle += ang3;
		return ang3;
	}
	else if( verAngle >= -20 && ang3 < 0 )
	{
		// 회전 허용.
		verAngle += ang3;
		return ang3;
	}
	else
	{
		// 회전 허용x.
		return 0;
	}
}