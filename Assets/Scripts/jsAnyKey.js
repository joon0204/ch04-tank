﻿#pragma strict
/* 필요 없어젔지만 혹시나 쓸까봐 남겨둠.
function Start()
{
	if( Application.loadedLevelName == "WinGame" )			// 씬 이름이 WinGame 일때 실행.
	{
		AudioSource.PlayClipAtPoint( gameWin, transform.position );
	}
	else if( Application.loadedLevelName == "LostGame" )	// 씬 이름이 LostGame 일때 실행.
	{
		AudioSource.PlayClipAtPoint( gameOver, transform.position );
	}
}
*/
function Update () {
	// MainGame 씬이면 실행.
	if( Application.loadedLevelName == "MainGame" )
	{
	// enter 키 누르면 Game씬으로 이동.
		if( Input.GetKey( KeyCode.Return ) )
		{
			Application.LoadLevel( "Game" );
		}
	}
	// MainGame 씬이 아닐때 실행.
	else
	{
	// 아무키나 눌르면 Main화면으로 이동.
		if( Input.anyKeyDown )
		{
			Application.LoadLevel( "MainGame" );
		}
	}
}