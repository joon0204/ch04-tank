﻿#pragma strict

private var wallHp : int = 0;		// 벽의 체력 설정.
private var time : float = 0.0f;
private var isCrash : boolean = false;
var text3D : GameObject;
private var myText;

function Start () {
	wallHp = 300;					// 초기 체력 300.
}

function Update()
{
	if( isCrash )
	{
		time += Time.deltaTime;
	}
	if( time > 0.5 )	// 0.5초 후 파괴.
	{
		time = 0.0f;
		isCrash = false;
		Destroy( myText );
		Debug.Log( "Destroy" );
	}
}

function OnTriggerEnter ( coll : Collider ) 
{
	if( coll.transform.tag == "BULLET" )
	{
		Debug.Log( wallHp );
		if( !isCrash )
		{
			isCrash = true;
			// 공격 받으면 남은 체력 텍스트 띄우기.
			text3D.GetComponent.< TextMesh >().text = wallHp.ToString();				// text 내용을 체력으로 바꿈.
			myText = Instantiate( text3D, transform.position, Quaternion.identity );	// 부딪힌 위치에 체력 표시.
		}
		
	}
}

// 소멸자 같은 역할을 함. 외부 스크립트에서 이 객체를 지웠을 떄. 텍스트가 지워지지 않는 것을 막아줌.
function OnDestroy()
{
	Destroy( myText );
	Debug.Log( "Destroy" );
}

function SetWallHp( attack : int )
{
	wallHp -= attack;		// 공격 받은 만큼 체력을 깎는다.
}

function GetWallHp()
{
	return wallHp;			// 현재 체력 반환.
}

function IsCrash()
{
	return isCrash;			// 충돌 상태 반환.
}