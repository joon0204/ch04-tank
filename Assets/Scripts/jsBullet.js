﻿#pragma strict

var snd : AudioClip;		// 포탄 사운드 파일.
var explosion : Transform;	// 파티클 파일.
var explosion2 : Transform;

function OnTriggerEnter( coll : Collider )
{
	Instantiate( explosion, gameObject.transform.position, Quaternion.identity );
	AudioSource.PlayClipAtPoint( snd, transform.position );	// 사운드 출력.
	if( coll.gameObject.tag == "WALL" && coll.GetComponent( jsWallHp ).IsCrash() == false )
	{
		coll.GetComponent( jsWallHp ).SetWallHp( 100 );			// jsWallHp의 함수를 불러옴.! 아핳아항희으ㅏ히ㅡ미허ㅣㅁ엏 구글 짱짱맨.
		if( coll.GetComponent( jsWallHp ).GetWallHp() <= 0 )	// 벽의 내구도가 다 줄어들면 터짐.
			Destroy( coll.gameObject );	// 장애뭄 제거.
	}
	else if( coll.gameObject.tag == "ENEMY" )	// ENEMY 를 맞췃을 때.
	{
		jsTankStatus.enemyHp -= 50;				// 50씩 체력을 깎는다.
		
		// 적 체력이 0 보다 작거나 같 을떄 게임 이김.
		if( jsTankStatus.enemyHp <= 0 )
		{
			Destroy( coll.gameObject );
			Application.LoadLevel( "WinGame" );
			
		}
	}
	else if( coll.gameObject.tag == "TANK" )	// TANK 를 맞췃을 때.
	{
		jsTankStatus.myHp -= 50;				// 50씩 체려을 깎는다.
		
		// 내 체력이 0보다 작거나 같을 때 게임 짐.
		if( jsTankStatus.myHp <= 0 )
		{
			Destroy( coll.gameObject );
			Application.LoadLevel( "LostGame" );
		}
	}
	Instantiate( explosion2, gameObject.transform.position, Quaternion.identity );	// 포탄이 터진 곳에 화염 파티클 생성.
	Destroy( gameObject );		// 포탄 제거.
}