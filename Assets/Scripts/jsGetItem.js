﻿#pragma strict

var snd : AudioClip;				// 먹을 때 나는 소리.
private var hpRecovery : int = 100;	// 회복량.

function OnTriggerEnter ( coll : Collider ) {
	//Debug.Log( coll.gameObject.tag );	// 부딪힌 물체 이름 출력.
	
	// 탱크나 적 탱크에 닿으면 실행.
	if( coll.gameObject.tag == "TANK" )
	{
		AudioSource.PlayClipAtPoint( snd, gameObject.transform.position );	// 뚀로리료레링!. 먹은 위치에서 소리가 들림.
		--jsHealth.itemCount;				// 먹었으므로 아이템 갯수 -- 
		jsTankStatus.myHp += hpRecovery;	// 체력 100 회복.
		
		// 최대 체력보다 커지는거 막음.
		if( jsTankStatus.myHp > jsTankStatus.MAXHP )
			jsTankStatus.myHp = jsTankStatus.MAXHP;
			
		Destroy( gameObject );	// 먹었으니까 없어짐!.
	}
	else if( coll.gameObject.tag == "ENEMY" )
	{
		AudioSource.PlayClipAtPoint( snd, gameObject.transform.position );	// 뚀로리료레링!.
		--jsHealth.itemCount;
		jsTankStatus.enemyHp += hpRecovery;
		
		// 최대 체력보다 커지는거 막음.
		if( jsTankStatus.enemyHp > jsTankStatus.MAXHP )
			jsTankStatus.enemyHp = jsTankStatus.MAXHP;
			
		Destroy( gameObject );	// 먹었으니까 없어짐!.
	}
}