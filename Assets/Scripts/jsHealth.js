﻿#pragma strict

var item : Transform;			// 아이템.

private var genTime : float = 0.0;		// 아이템 생성 시간.
private var MAXGENERATE : int = 2;		// 최대 생성 갯수.
static var itemCount : int = 0;			// 생성된 아이템 갯수.

function Start()
{
// 새로 시작하면 초기화 시킴. 다시 시작 했을 때 아이템이 생성되지 않는 현상 방지.
	genTime = 0.0f;
	itemCount = 0;
}

function Update()
{
	// 아이템이 최대로 생성됬을때도 카운트가 되서 아이템 먹자마자 바로 생성되는거 방지.
	if( MAXGENERATE > itemCount )
		genTime += Time.deltaTime;
		
	// 10초가 지나지 않았거나 아이템 수가 최대 아이템 수보다 많거나 같을 떄 아무것도 안함.
	if( genTime < 10 || MAXGENERATE <= itemCount )
	{
		return;
	}
	else
	{
		var randVec : Vector3 = Vector3( Random.Range( -30.0f, 30.0f ), 0, Random.Range( -30.0f, 30.0f ) );	// 랜덤의 위치 저장 시킴.
		Instantiate( item, randVec, Quaternion.identity );	// randVec 자리에 item을 생성.
		++itemCount;		// 아이템이 하나 생겼으므로 ++ 해줌.
	
		genTime = 0.0f;		// 시간 리셋.
	}
}

// 시간을 초기화 시킴. 외부에서 사용하기 위해 만듬.
function TimeReset()
{
	genTime = 0.0f;
}