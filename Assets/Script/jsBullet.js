﻿#pragma strict

var snd : AudioClip;		// 사운드 파일.
var explosion : Transform;	// 파티클 파일.
var explosion2 : Transform;

function OnTriggerEnter( coll : Collider )
{
	Instantiate( explosion, gameObject.transform.position, Quaternion.identity );
	//AudioSource.PlayClipAtPoint( snd, transform.position );	// 사운드 출력.
	if( coll.gameObject.tag == "WALL" )
	{
		Destroy( coll.gameObject );	// 장애뭄 제거.
		Destroy( gameObject );		// 포탄 제거.
	}
	else if( coll.gameObject.tag == "Untagged" )
	{
		Instantiate( explosion2, gameObject.transform.position, Quaternion.identity );
		Destroy( gameObject );
	}
	else if( coll.gameObject.tag == "ENEMY" )
	{
		++jsScore.hit;
		if( jsScore.hit > 5 )
		{
			Destroy( coll.gameObject );
			Application.LoadLevel( "WinGame" );
			
		}
	}
	else if( coll.gameObject.tag == "TANK" )
	{
		++jsScore.lose;
		if( jsScore.lose > 5 )
		{
			Destroy( coll.gameObject );
			Application.LoadLevel( "LostGame" );
		}
	}
}