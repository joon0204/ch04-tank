﻿#pragma strict

private var power = 1200;

var bullet		: Transform;
var target		: Transform;
var spPoint		: Transform;
var explosion	: Transform;
var snd			: AudioClip;


function Update () {
	transform.LookAt( target );
	
	var hit : RaycastHit;		// 탐색 결과 저장.
	//var fwd = Vector3.forward;	// 포탑의 전방. 로컬좌표.
	var fwd = transform.TransformDirection( Vector3.forward );	// 글로벌좌표
	
	Debug.DrawRay( spPoint.transform.position, fwd * 20, Color.green );
	
	// 탐색 실패.
	if( Physics.Raycast( spPoint.transform.position, fwd, hit, 20 ) == false ) return ;
	
	Debug.Log( hit.collider.gameObject.name );
	
	if( hit.collider.gameObject.tag != "TANK" ) return;
	
	// 포구 앞의 화염
	Instantiate( explosion, spPoint.transform.position, Quaternion.identity );
	
	// 포탄.
	var obj = Instantiate( bullet, spPoint.transform.position, Quaternion.identity );
	obj.rigidbody.AddForce( fwd * power );
	
	//AudioSource.PlayClipAtPoint( snd, spPoint.transform.position );
}