﻿#pragma strict

var snd : AudioClip;		// 사운드 파일.
var explosion : Transform;	// 파티클 파일.
var explosion2 : Transform;

function OnTriggerEnter( coll : Collider )
{
	if( coll.gameObject.tag == "WALL" )
	{
		Instantiate( explosion, gameObject.transform.position, Quaternion.identity );
		//AudioSource.PlayClipAtPoint( snd, transform.position );	// 사운드 출력.
		Destroy( coll.gameObject );	// 장애뭄 제거.
		Destroy( gameObject );		// 포탄 제거.
	}
	if( coll.gameObject.tag == "Untagged" )
	{
		Instantiate( explosion2, gameObject.transform.position, Quaternion.identity );
		Destroy( gameObject );
	}
}